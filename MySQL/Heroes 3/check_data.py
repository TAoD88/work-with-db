import os

### Прверяем и забираем последний файл который был внесен последним
def check_json(name) -> str:
    path = f"data/{name}"
    # Создаем список названий файлов в указанной нами папки который содержит все файлы формата "json"
    file_list = [i for i in os.listdir(path) if i.endswith("json")]
    for file in file_list:
        if name in file:
            file_pars = file  # Находим последний добавленный файл пробегаясь циклом по списку
            break
    # возращаем название файла
    return file_pars
