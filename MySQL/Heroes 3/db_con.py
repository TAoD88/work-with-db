import mysql
from mysql.connector import Error


### Создаем класс Database
class Database:
    def __init__(self, user_name, user_password, host_name, db_name):
        self.connection = mysql.connector.connect(
            user=user_name,
            password=user_password,
            host=host_name,
            database=db_name
        )

    # Создаем таблицы если еще не созданы и добавляем колонки
    def name_table(self, name, list_table):
        _cursor = self.connection.cursor()
        create_table_name = '''
                CREATE TABLE IF NOT EXISTS {} (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (id) );'''.format(name)
        _cursor.execute(create_table_name)
        self.connection.commit()
        # Создаем колонки из списка и всем присваиваем LONGTEXT так как мне лень для каждого типа данных делать свою
        for col_name in list_table:
            column_name = "ALTER TABLE {} ADD COLUMN {} LONGTEXT NOT NULL;".format(name, col_name)
            try:
                _cursor.execute(column_name)
                self.connection.commit()
            except Error as er:
                print(er)
                self.connection.rollback()

        _cursor.close()

    # Сохраняем данные в подготовленную таблицу
    def data_save(self, name, list_table, data):
        _cursor = self.connection.cursor()
        count = 0  # Создаем счетчик для проверки кол-ва
        str_table = ', '.join(str(x) for x in list_table)  # ОЧЕНЬ ВАЖНО!!! правильно записать строкой имя столбцов
        #
        for i in data:
            # ОЧЕНЬ ОЧЕНЬ ВАЖНО!!! при дабовлении VALUES в таблицу необходимо того что это должна быть строка,
            # так еще каждый элемент должем быть в кавычках ('элемент') иначе не будет заносить данные и выдавать ошибку
            str_data = ', '.join("'" + str(x) + "'" for x in i.values())
            count += 1
            data = '''INSERT INTO %s (%s) VALUES (%s);''' % (name, str_table, str_data)
            try:
                _cursor.execute(data)
                self.connection.commit()
                print(f' Good: {i["engname"]} {count}')
            except Error as er:
                self.connection.rollback()
                # print(f' Bed: {i["engname"]} {count}')
                # print(f' Bed: {er}')
        _cursor.close()
