from data_to_sql import json_to_sql
import time
from config import load_config, Config
from db_con import Database

### Запускаем и радуемся если с первого раза сработает =)))
def main():
    start_time = time.time()
    # Создаем список для работы
    list_names = ['mobs', 'artifacts', 'heroes', 'spells']
    # пробегаемся по каждому элементу списка
    for name in list_names:
        json_to_sql(name, db)

    print('Все таблицы созданы. Все данные занесены в Таблицы')
    full_time = time.time() - start_time
    print(f'На работу скрипта потрачено: {full_time} сек.')


if __name__ == "__main__":
    config: Config = load_config(path='.env')
    db = Database(config.db.db_user,
                  config.db.db_password,
                  config.db.db_host,
                  config.db.database
                  )
    main()
