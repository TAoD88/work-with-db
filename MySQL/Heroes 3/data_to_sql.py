from check_data import check_json
import json


### Возвращаем только имена для колонок в БД
def colums_from_table(file, name) -> list:
    with open(f'data/{name}/{file}', encoding='UTF-8') as rf:
        data = json.load(rf)
        return [name for name in data[0].keys()]


### Возвращаем все данные из файла
def data_from_table(file, name) -> list:
    with open(f'data/{name}/{file}', encoding='UTF-8') as rf:
        data = json.load(rf)
        return data


### Создаем таблицы и заносим данные
def creat_table(name, list_table, db, data):
    db.name_table(name, list_table) # пользуемся name_table методом класса и создаем таблицы
    db.data_save(name, list_table, data) # # пользуемся data_save методом класса и заносим данные в БД


### Основная функция вылоняющая создание таблиц в БД и занесение в таблицы данных
def json_to_sql(name, db):
    file = check_json(name) # получаем имя файла ()
    colums_name = colums_from_table(file, name) # получаем список для записи колонок
    data_for_table = data_from_table(file, name) # получаем лист данных для записи
    creat_table(name, colums_name, db, data_for_table) # Передаем все в функцию и записываем все в БД
